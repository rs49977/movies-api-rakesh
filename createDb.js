const path = require("path");
const fs = require("fs");

const conn = require("./db/config");

const moviesDataPath = path.resolve(__dirname, "./public/movies.json");

const getItems = (moviesJson, item) => {
  return [...new Set(moviesJson.map((movieInfo) => movieInfo[item]))];
};

// const getMovies = (genres, actors, directors, moviesJson) => {
//   return moviesJson.map((movieInfo) => {
//     return {
//       ...movieInfo,
//       Genre: genres.indexOf(movieInfo.Genre),
//       Director: directors.indexOf(movieInfo.Director),
//       Actor: actors.indexOf(movieInfo.Actor),
//     };
//   });
// };

const inserQueryHandler = (conn, query, data) => {
  return conn.query(query, [data], (err, result) => {
    if (err) throw err;
    else {
      console.log("successfully inserted");
      return result;
    }
  });
};

const createQueryHandler = (conn, query) => {
  return conn.query(query, (err, result) => {
    if (err) throw err;
    else {
      console.log("creeated successfully !!");
      return result;
    }
  });
};

async function createIplDatabase() {
  try {
    const moviesJson = JSON.parse(
      await fs.promises.readFile(moviesDataPath, "utf-8")
    );

    const genres = getItems(moviesJson, "Genre");
    const actors = getItems(moviesJson, "Actor");
    const directors = getItems(moviesJson, "Director");
    // const movies = getMovies(genres, actors, directors, moviesJson);
    const movies = moviesJson;

    // console.log(genres, actors, directors, movies);

    const createMoviesDbQry = "CREATE DATABASE IF NOT EXISTS movies";
    const dbResult = createQueryHandler(conn, createMoviesDbQry);
    conn.changeUser({ database: "movies" }, function (err) {
      if (err) throw err;
    });

    const createMovieTableQry =
      `CREATE TABLE IF NOT EXISTS movie (` +
      "`rank` INT PRIMARY KEY," + // we cann't use rank in column name
      `Title VARCHAR(50),
            description VARCHAR(500),
            runtime INT,
            genre VARCHAR(50),
            rating FLOAT(2),
            metascore INT,
            votes INT,
            gross_earning_in_mil FLOAT(2),
            Director VARCHAR(100),
            Actor VARCHAR(100),
            Year INT
            );`;
    const createMovieResult = createQueryHandler(conn, createMovieTableQry);

    const insertMovieQry = `INSERT INTO movie VALUES ?`;
    // insert alawys accept array of array
    const movieInsertResult = inserQueryHandler(
      conn,
      insertMovieQry,
      movies.map((movieInfo) => Object.values(movieInfo))
    );

    const createGenreTableQry = `CREATE TABLE IF NOT EXISTS genre ( genre_id INT AUTO_INCREMENT PRIMARY KEY, genre_name VARCHAR(50) );`;
    const createGenreResult = createQueryHandler(conn, createGenreTableQry);

    const insertGenreQry = `INSERT INTO genre (genre_name) VALUES ?`;
    const genreInsertResult = inserQueryHandler(
      conn,
      insertGenreQry,
      genres.map((genre) => [genre])
    );

    const createActorTableQry = `CREATE TABLE IF NOT EXISTS actor ( actor_id INT AUTO_INCREMENT PRIMARY KEY, actor_name VARCHAR(50) );`;
    const createActorResult = createQueryHandler(conn, createActorTableQry);

    const insertActorQry = `INSERT INTO actor (actor_name) VALUES ?`;
    const actorInsertResult = inserQueryHandler(
      conn,
      insertActorQry,
      actors.map((actor) => [actor])
    );

    const createDirectorTableQry = `CREATE TABLE IF NOT EXISTS director ( director_id INT AUTO_INCREMENT PRIMARY KEY, director_name VARCHAR(50) );`;
    const createDirectorResult = createQueryHandler(
      conn,
      createDirectorTableQry
    );

    const insertDirectorQry = `INSERT INTO director (director_name) VALUES ?`;
    const directorInsertResult = inserQueryHandler(
      conn,
      insertDirectorQry,
      directors.map((director) => [director])
    );

    conn.end();
  } catch (err) {
    console.log("Some problem :" + err);
  }
}

createIplDatabase();
