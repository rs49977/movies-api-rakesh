const mysql = require("mysql");
const dotenv = require("dotenv");

const result = dotenv.config();
if (result.error) console.log(result.parsed);

const conn = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
});

conn.connect((err) => {
  if (err) throw err;
  console.log("database connected successfully !!");
});

module.exports = conn;
