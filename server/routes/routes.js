const model = require("../models/movies");

module.exports = (app) => {
  app.get("/", (req, res) => {
    res.send("Hello world !");
  });

  // to manage movies entire collection
  app.get("/api/movies", (req, res) => {
    model.getMovies((err, movies) => {
      if (err) {
        res.send(err);
      } else {
        res.send(movies);
      }
    });
  });

  app.post("/api/movies", (req, res) => {
    if (Object.entries(req.body).length <= 7) {
      console.log("Error in sending data");
      res.status(400).send("Insufficient data!");
    } else {
      model.addMovie([Object.values(req.body)], (err, movie) => {
        if (err) {
          console.log("having some err");
          res.send(err);
        } else {
          console.log("added succefully !!");
          res.send(movie);
        }
      });
    }
  });

  // // to manage single movie
  app.get("/api/movies/:id", (req, res) => {
    model.getMovie(req.params.id, (err, movie) => {
      if (err) {
        console.log("having some err");
        res.send(err);
      } else {
        console.log("retrive succefully !!");
        res.send(movie);
      }
    });
  });
  app.put("/api/movies/:id", (req, res) => {
    if (Object.entries(req.body).length <= 7) {
      console.log("Error in sending data");
      res.status(400).send("Insufficient data!");
    } else {
      model.updateMovie(
        req.params.id,
        Object.values(req.body),
        (err, movie) => {
          if (err) {
            console.log("having some err");
            res.send(err);
          } else {
            console.log("added succefully !!");
            res.send(movie);
          }
        }
      );
    }
  });

  app.delete("/api/movies/:id", (req, res) => {
    model.deleteMovie(req.params.id, (err, movie) => {
      if (err) {
        console.log("having some err");
        res.send(err);
      } else {
        console.log("retrive succefully !!");
        res.send(movie);
      }
    });
  });

  // // to manage entire directors collection
  app.get("/api/directors", (req, res) => {
    model.getDirectors((err, directors) => {
      if (err) {
        console.log("having some err");
        res.send(err);
      } else {
        console.log("retrive succefully !!");
        res.send(directors);
      }
    });
  });

  app.post("/api/directors", (req, res) => {
    if (Object.entries(req.body).length === 0) {
      console.log("Error in sending data");
      res.status(400).send("Insufficient data!");
    } else {
      model.addDirector([[req.body.name]], (err, director) => {
        if (err) {
          console.log("having some err");
          res.send(err);
        } else {
          console.log("added succefully !!");
          res.send(director);
        }
      });
    }
  });

  //  manage single director
  app.get("/api/directors/:id", (req, res) => {
    model.getDirector(req.params.id, (err, director) => {
      if (err) {
        console.log("having some err");
        res.send(err);
      } else {
        console.log("retrive succefully !!");
        res.send(director);
      }
    });
  });

  app.put("/api/directors/:id", (req, res) => {
    if (Object.entries(req.body).length === 0) {
      res.status(400).send("nothing to update!");
    } else {
      model.updateDirector(req.params.id, req.body.name, (err, director) => {
        if (err) {
          console.log("having some err");
          res.send(err);
        } else {
          console.log("added succefully !!");
          res.send(director);
        }
      });
    }
  });

  app.delete("/api/directors/:id", (req, res) => {
    model.deleteDirector(req.params.id, (err, info) => {
      if (err) {
        console.log("having some err");
        res.send(err);
      } else {
        console.log("retrive succefully !!");
        res.send(info);
      }
    });
  });
};
