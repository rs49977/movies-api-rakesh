const conn = require("../../db/config");
conn.changeUser({ database: "movies" }, function (err) {
  if (err) throw err;
});

const getMovies = (result) => {
  conn.query(
    // "select `rank`, title, description, runtime, genre_name as genre, rating, metascore, votes, gross_earning_in_mil, director_name as director, actor_name as actor, year from movie join genre on genre.genre_id = movie.genre join actor on actor.actor_id = movie.actor join director on director.director_id = movie.director ;",
    "select * from movie;",
    (err, res) => {
      if (err) {
        console.log(err);
        result(null, err);
      } else {
        console.log("fetched successfully !!");
        result(null, res);
      }
    }
  );
};

const addMovie = (data, result) => {
  conn.query("insert into movie  values ?;", [data], (err, res) => {
    if (err) {
      console.log(err);
      result(null, err);
    } else {
      console.log("inserted successfully !!");
      result(null, res);
    }
  });
};

const getMovie = (id, result) => {
  conn.query("select * from movie where `rank` = ?", id, (err, res) => {
    if (err) {
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

const updateMovie = (id, data, result) => {
  conn.query(
    "update movie set title = ?, description = ?, runtime = ?, genre = ?, rating = ?, metascore = ?, votes = ?, gross_earning_in_mil = ?, director = ?, actor = ?, year = ? where `rank` = ?;",
    [...data.map((item) => [item]), [id]],
    (err, res) => {
      if (err) {
        console.log(err);
        result(null, err);
      } else {
        console.log("fetched successfully !!");
        result(null, res);
      }
    }
  );
};

const deleteMovie = (id, result) => {
  conn.query("delete from movie where `rank` = ?", id, (err, res) => {
    if (err) {
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

// directors data manipulating method
const getDirectors = (result) => {
  conn.query("select director_name as name from director;", (err, res) => {
    if (err) {
      console.log(err);
      result(null, err);
    } else {
      console.log("fetched successfully !!");
      result(null, res);
    }
  });
};

const getDirector = (id, result) => {
  conn.query(
    "select director_name as name from director where director_id = ?;",
    id,
    (err, res) => {
      if (err) {
        console.log(err);
        result(null, err);
      } else {
        console.log("fetched successfully !!");
        result(null, res);
      }
    }
  );
};

const addDirector = (data, result) => {
  conn.query(
    "insert into director (director_name) values ?;",
    [data],
    (err, res) => {
      if (err) {
        console.log(err);
        result(null, err);
      } else {
        console.log("fetched successfully !!");
        result(null, res);
      }
    }
  );
};

const updateDirector = (id, data, result) => {
  conn.query(
    `update director set director_name='${data}' where director_id = ${id};`,
    (err, res) => {
      if (err) {
        console.log(err);
        result(null, err);
      } else {
        console.log("fetched successfully !!");
        result(null, res);
      }
    }
  );
};

const deleteDirector = (id, result) => {
  conn.query("delete from director where director_id = ?;", id, (err, res) => {
    if (err) {
      console.log(err);
      result(null, err);
    } else {
      console.log("fetched successfully !!");
      result(null, res);
    }
  });
};

module.exports = {
  getMovies,
  addMovie,
  getMovie,
  updateMovie,
  deleteMovie,
  getDirectors,
  getDirector,
  addDirector,
  deleteDirector,
  updateDirector,
};

// title,
//   description,
//   runtime,
//   genre,
//   rating,
//   metascore,
//   votes,
//   gross_earning_in_mil,
//   director,
//   actor,
//   year;
