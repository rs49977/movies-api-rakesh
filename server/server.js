const express = require("express");
const dotenv = require("dotenv");
const bodyParser = require("body-parser");

const routes = require("./routes/routes.js");

const result = dotenv.config();
if (result.error) console.log(result.parsed);

const port = process.env.PORT;

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

routes(app);

app.listen(port, (err) => {
  const msg = err ? err : "Server running on port: " + port;
  console.log(msg);
});
